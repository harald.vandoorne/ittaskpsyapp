﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsyApp
{
    /// <summary>
    /// Interaction logic for PatientDetailWindow.xaml
    /// </summary>
    public partial class PatientDetailWindow : Window
    {
        Patient myPatient = new Patient();
        List<Session> mySessions = new List<Session>();
        public PatientDetailWindow(Patient x)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.myPatient = x;
            this.mySessions = DataManager.GetSessionsByPatient(x);
            mySessions.OrderBy(p => p.SessionDate);
            cmbCompany.Items.Add("Privé");
            cmbCompany.Items.Add("Bedrijf");
            UpdateInput(myPatient);
        }

        private void btnPatientMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAddSession_Click(object sender, RoutedEventArgs e)
        {
            SessionCreateWindow mySessionCreateWindow = new SessionCreateWindow(myPatient);
            mySessionCreateWindow.ShowDialog();
            if (mySessionCreateWindow.DialogResult.HasValue && mySessionCreateWindow.DialogResult.Value)
            {
                MessageBox.Show($"Sessie Aangemaakt voor Patient", "Succes", MessageBoxButton.OK);
                lbSessions.ItemsSource = null;
                mySessions = DataManager.GetSessionsByPatient(myPatient);
                lbSessions.ItemsSource = mySessions;
            }
        }
        private void btnDetailSession_Click(object sender, RoutedEventArgs e)
        {
            SessionDetailWindow mySessionDetailWindow = new SessionDetailWindow(lbSessions.SelectedItem as Session);
            mySessionDetailWindow.ShowDialog();

        }
        private void UpdateInput(Patient x)
        {
            txtCity.Text = x.PatientCity.ToString();
            txtStreet.Text = x.PatientStreet.ToString();
            txtVoornaam.Text = x.PatientFirstName.ToString();
            txtFamilienaam.Text = x.PatientLastName.ToString();
            txtNumber.Text = x.PatientHouseNumber.ToString();
            txtPhone.Text = x.PatientPhone.ToString();
            txtEmail.Text = x.PatientEmail.ToString();
            txtReferral.Text = x.PatientReferral.ToString();
            txtReason.Text = x.PatientReferralReason.ToString();
            txtZip.Text = x.PatientZipcode.ToString();
            if (x.PatientComments != null)
            {
                txtComments.Text = x.PatientComments.ToString();
            }          
            lbSessions.ItemsSource = mySessions;
            if (x.PatientIsCompany)
            {
                cmbCompany.SelectedIndex = 1;
                txtCompany.Text = x.PatientCompanyNumber.ToString();
            }
            else
            {
                cmbCompany.SelectedIndex = 0;
            }
            
        }
    }
}
