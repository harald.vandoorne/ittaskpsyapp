//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PsyApp
{
    using System;
    using System.Collections.Generic;
    
    public partial class Patient
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Patient()
        {
            this.Session = new HashSet<Session>();
        }
    
        public int PatientID { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public string PatientStreet { get; set; }
        public string PatientHouseNumber { get; set; }
        public string PatientZipcode { get; set; }
        public string PatientCity { get; set; }
        public string PatientPhone { get; set; }
        public string PatientEmail { get; set; }
        public string PatientReferral { get; set; }
        public string PatientReferralReason { get; set; }
        public string PatientComments { get; set; }
        public bool PatientIsCompany { get; set; }
        public string PatientCompanyNumber { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Session> Session { get; set; }
    }
}
