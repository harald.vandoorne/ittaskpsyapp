﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsyApp
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public User myUser = new User();
        public Login(User myUser)
        {
            InitializeComponent();
            this.myUser = myUser;

            //for testing ease
            txtUsernameSignIn.Text = "Administrator1";

        }

        private void btnSignIn_Click(object sender, RoutedEventArgs e)
        {
            if (txtUsernameSignIn.Text != "")
            {
                // check if username exists
                if (DataManager.GetUser(txtUsernameSignIn.Text) != null)
                {
                    // get user object from existing username and compare passwords
                    User user = DataManager.GetUser(txtUsernameSignIn.Text);
                    if (user.UserName != null)
                    {
                        if (user.UserPassword == txtPasswordSignIn.Password)
                        {
                            EmptySignInFields();
                            this.DialogResult = true;
                            myUser = user;
                            this.Close();

                        }
                        else
                        {
                            lblMessages.Content = $"The password for user '{user.UserName}' is not correct.";
                        }
                    }                                       
                }
                else
                {
                    lblMessages.Content = $"User Does Not exist";
                }
            }
            else
            {
                lblMessages.Content = $"Please fill in a username.";
            }
        }
        private void EmptySignInFields()
        {
            txtUsernameSignIn.Text = null;
            txtPasswordSignIn.Password = null;
        }
    }
    
}
