﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsyApp
{
    /// <summary>
    /// Interaction logic for SessionUpdateWindow.xaml
    /// </summary>
    public partial class SessionUpdateWindow : Window
    {
        Session mySession = new Session();
        List<Day> myDays = new List<Day>();
        Day myDay = new Day();
        Patient myPatient = new Patient();
        List<Patient> myPatients = new List<Patient>();
        public SessionUpdateWindow(Session x)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            mySession = x;
            myDays = DataManager.GetAllDays();
            myDay = myDays.FirstOrDefault(y => y.DayID == mySession.DayID);
            myPatients = DataManager.GetAllPatients();
            myPatient = myPatients.FirstOrDefault(z => z.PatientID == mySession.PatientID);
            LoadData(mySession);
        }       
           
        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void LoadData(Session x)
        {
            dtpSelectDay.SelectedDate = mySession.SessionDate;
            CheckAvailableSlots(myDay);
            cmbPatientPicker.ItemsSource = myPatients;
            cmbPatientPicker.SelectedItem = myPatient;
            cmbHourPicker.SelectedItem = "Huidige Selectie";
            LoadComboBoxes();
            cmbPaymentStatus.SelectedIndex = cmbPaymentStatus.Items.IndexOf(mySession.SessionIsPaid.ToString() as string);
            cmbPaymentMethod.SelectedItem  = mySession.SessionPaymentMethod.ToString();
            cmbDuurtijd.SelectedItem = mySession.SessionDuration.ToString();
        }

          
        
        private void LoadComboBoxes()
        {
            cmbPaymentMethod.Items.Add("Cash");
            cmbPaymentMethod.Items.Add("Bankkaart");
            cmbPaymentMethod.Items.Add("Overschrijving");
            cmbPaymentMethod.Items.Add("Andere");
            cmbPaymentStatus.Items.Add("Niet Betaald");
            cmbPaymentStatus.Items.Add("Betaald");
            cmbDuurtijd.Items.Add("1 Uur");
            cmbDuurtijd.Items.Add("2 Uur");
            txtComments.Text = mySession.SessionComments;
            txtPrice.Text = mySession.SessionPrice.ToString();
            txtSubject.Text = mySession.SessionSubject;
        }
    }
}
