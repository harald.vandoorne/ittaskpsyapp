﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsyApp
{
    /// <summary>
    /// Interaction logic for SessionCreateWindow.xaml
    /// </summary>
    public partial class SessionCreateWindow : Window
    {
        Session mySession = new Session();
        Patient myPatient = new Patient();
        Day myDay = new Day();
        List<Day> myDays = new List<Day>();
        List<Patient> myPatients = new List<Patient>();
        List<Session> mySessions = new List<Session>();

        public SessionCreateWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            myDays = DataManager.GetAllDays();
            dtpSelectDay.SelectedDate = DateTime.Now;
            myPatients = DataManager.GetAllPatients();
            cmbPatientPicker.ItemsSource = myPatients;
            ResetButtons();
            LoadComboBoxes();
            
        }
        public SessionCreateWindow(Patient x)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            myPatient = x;
            myDays = DataManager.GetAllDays();
            mySession.Patient = x;            
            dtpSelectDay.SelectedDate = DateTime.Now;
            cmbPatientPicker.Items.Add(x);
            cmbPatientPicker.SelectedIndex = 0;
            ResetButtons();
            LoadComboBoxes();
        }

        public SessionCreateWindow(Day x)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.myDay = x;
            mySession.Day = x;
            myDays = DataManager.GetAllDays();
            dtpSelectDay.SelectedDate = myDay.DayDate;
            btnDateConfirm.IsEnabled = false;
            btnNextAvailableDate.IsEnabled = false;
            CheckAvailableSlots(myDay);
            ResetButtons();
            LoadComboBoxes();
        }

        private void btnDateConfirm_Click(object sender, RoutedEventArgs e)
        {
            bool dayExists = false;
            DateTime pickedDateTime = ((DateTime)((DateTime)dtpSelectDay.SelectedDate as DateTime?));
            var pickedDate = pickedDateTime.Date;
            foreach (Day x in myDays)
            {
                if (x.DayDate.Date == pickedDate)
                {
                    this.myDay = x;
                    dayExists = true;
                    mySession.Day = myDay;
                    mySession.DayID = myDay.DayID;
                    mySession.SessionDate = pickedDate;
                    CheckAvailableSlots(myDay);
                    cmbHourPicker.IsEnabled = true;
                    cmbPatientPicker.IsEnabled = true;
                    btnPatientConfirm.IsEnabled = true;
                }
            }
            if (!dayExists)
            {
                myDay.DayDate = pickedDate;
                DataManager.CreateDay(myDay);
                myDays = DataManager.GetAllDays();
                myDay = myDays.FirstOrDefault(x => x.DayDate.Date == pickedDate);
                mySession.Day = myDay;
                mySession.DayID = myDay.DayID;
                mySession.SessionDate = pickedDate;
                CheckAvailableSlots(myDay);
                cmbHourPicker.IsEnabled = true;
                cmbPatientPicker.IsEnabled = true;
                btnPatientConfirm.IsEnabled = true;
            }
        }

        private void btnNextAvailableDate_Click(object sender, RoutedEventArgs e)
        {
            // finds next available date with free slots
        }
       
        private void btnPatientConfirm_Click(object sender, RoutedEventArgs e)
        {
            if (cmbPatientPicker.SelectedIndex != -1 && cmbHourPicker.SelectedIndex != -1 && (string)cmbHourPicker.SelectedItem != "Tijdsslot Bezet")
            {

                myPatient = cmbPatientPicker.SelectedItem as Patient;
                mySession.Patient = myPatient;
                mySession.PatientID = myPatient.PatientID;
                cmbDuurtijd.IsEnabled = true;
                cmbPaymentMethod.IsEnabled = true;
                cmbPaymentStatus.IsEnabled = true;
                txtPrice.IsEnabled = true;
                txtSubject.IsEnabled = true;
                txtComments.IsEnabled = true;
                cmbHourPicker.IsEnabled = false;
                cmbPatientPicker.IsEnabled = false;
                dtpSelectDay.IsEnabled = false;
                btnNextAvailableDate.IsEnabled = false;
                btnDateConfirm.IsEnabled = false;
                btnPatientConfirm.IsEnabled = false;
                btnCreate.IsEnabled = true;
            }
            else
            {
                MessageBox.Show("Gelieve een Patient of een (ander) Tijdslot te kiezen", "Error", MessageBoxButton.OK);
            }
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            mySession.SessionComments = txtComments.Text;
            mySession.SessionSubject = txtSubject.Text;
            mySession.SessionDuration = cmbDuurtijd.SelectedIndex + 1;
            Decimal.TryParse(txtPrice.Text, out decimal myPrice);
            mySession.SessionPrice = myPrice;
            if (cmbPaymentStatus.SelectedIndex == 0)
            {
                mySession.SessionIsPaid = false;
            }
            else
            {
                mySession.SessionIsPaid = true;
            }
            mySession.SessionPaymentMethod = cmbPaymentMethod.Text;
            switch (cmbHourPicker.SelectedIndex)
            {
                case 0:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);                   
                    break;
                case 1:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                case 2:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                case 3:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                case 4:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                case 5:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                case 6:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                case 7:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                case 8:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                case 9:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                case 10:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                case 11:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                case 12:
                    mySession.SessionStart = myDay.DayDate.AddHours(cmbHourPicker.SelectedIndex + 8);
                    break;
                default:
                    break;
            }
            DataManager.CreateSession(mySession);
            mySessions = DataManager.GetAllSessions();                      
            DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void CheckAvailableSlots(Day x)
        {
            cmbHourPicker.Items.Clear();
                       
            
        }

        private void ResetButtons()
        {
            cmbHourPicker.IsEnabled = false;
            cmbPatientPicker.IsEnabled = false;
            btnPatientConfirm.IsEnabled = false;
            cmbDuurtijd.IsEnabled = false;
            txtPrice.IsEnabled = false;
            txtComments.IsEnabled = false;
            txtSubject.IsEnabled = false;
            btnCreate.IsEnabled = false;
            cmbPaymentMethod.IsEnabled = false;
            cmbPaymentStatus.IsEnabled = false;
        }

        private void LoadComboBoxes()
        {
            cmbPaymentMethod.Items.Add("Cash");
            cmbPaymentMethod.Items.Add("Bankkaart");
            cmbPaymentMethod.Items.Add("Overschrijving");
            cmbPaymentMethod.Items.Add("Andere");
            cmbPaymentStatus.Items.Add("Niet Betaald");
            cmbPaymentStatus.Items.Add("Betaald");
            cmbDuurtijd.Items.Add("1 Uur");
            cmbDuurtijd.Items.Add("2 Uur");
        }
    }
}
