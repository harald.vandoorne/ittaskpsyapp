﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsyApp
{
    /// <summary>
    /// Interaction logic for SessionDetailWindow.xaml
    /// </summary>
    public partial class SessionDetailWindow : Window
    {
        Session mySession = new Session();
        List<Patient> myPatients = new List<Patient>();
        Patient myPatient = new Patient();
        public SessionDetailWindow(Session x)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            mySession = x;
            dtpSelectDay.SelectedDate = mySession.SessionDate;
            myPatients = DataManager.GetAllPatients();
            myPatient = myPatients.FirstOrDefault(y => y.PatientID == mySession.PatientID);
            txtPatientPicker.Text = myPatient.ToString();
            txtDuurtijd.Text = mySession.SessionDuration.ToString();
            txtPaymentStatus.Text = mySession.SessionIsPaid.ToString();
            txtPaymentMethod.Text = mySession.SessionPaymentMethod;
            String hourMinuteStart = mySession.SessionStart.ToString("HH:mm");
            txtHourPicker.Text = hourMinuteStart;
            if (mySession.SessionEnd != null)
            {
                String hourMinuteSTart = mySession.SessionStart.ToString("HH:mm");
                //txt HOUR END insteken
            }
            txtComments.Text = mySession.SessionComments;
            txtPrice.Text = mySession.SessionPrice.ToString();
            txtSubject.Text = mySession.SessionSubject;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }      
    }
}
