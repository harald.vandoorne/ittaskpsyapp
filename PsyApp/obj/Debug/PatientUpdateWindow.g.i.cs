﻿#pragma checksum "..\..\PatientUpdateWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "D5102727B1DB0B20D60BD1BB82C4BCC58D53DFBE1162A8D7F1751E4751DE18BF"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using PsyApp;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PsyApp {
    
    
    /// <summary>
    /// PatientUpdateWindow
    /// </summary>
    public partial class PatientUpdateWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 18 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtVoornaam;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFamilienaam;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtStreet;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNumber;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtCity;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtZip;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPhone;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtEmail;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbCompany;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtCompany;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtReferral;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtReason;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtComments;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lbSessions;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSavePatient;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPatientMenu;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAddSession;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\PatientUpdateWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDetailSession;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PsyApp;component/patientupdatewindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\PatientUpdateWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.txtVoornaam = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.txtFamilienaam = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.txtStreet = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.txtNumber = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.txtCity = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.txtZip = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.txtPhone = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.txtEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.cmbCompany = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 10:
            this.txtCompany = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.txtReferral = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.txtReason = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.txtComments = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.lbSessions = ((System.Windows.Controls.ListBox)(target));
            return;
            case 15:
            this.btnSavePatient = ((System.Windows.Controls.Button)(target));
            
            #line 60 "..\..\PatientUpdateWindow.xaml"
            this.btnSavePatient.Click += new System.Windows.RoutedEventHandler(this.btnSavePatient_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.btnPatientMenu = ((System.Windows.Controls.Button)(target));
            
            #line 61 "..\..\PatientUpdateWindow.xaml"
            this.btnPatientMenu.Click += new System.Windows.RoutedEventHandler(this.btnPatientMenu_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.btnAddSession = ((System.Windows.Controls.Button)(target));
            
            #line 62 "..\..\PatientUpdateWindow.xaml"
            this.btnAddSession.Click += new System.Windows.RoutedEventHandler(this.btnAddSession_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.btnDetailSession = ((System.Windows.Controls.Button)(target));
            
            #line 63 "..\..\PatientUpdateWindow.xaml"
            this.btnDetailSession.Click += new System.Windows.RoutedEventHandler(this.btnDetailSession_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

