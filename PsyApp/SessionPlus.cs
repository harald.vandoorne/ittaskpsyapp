﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PsyApp
{
    public partial class Session
    {
        public override string ToString()
        {
            return $"{SessionDateTime}       {SessionSubject}";
        }
    }
}
